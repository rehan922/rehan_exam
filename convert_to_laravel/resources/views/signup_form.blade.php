@extends('layout.web')

@section('content')
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7 bg-img">
                <h1 class="logo">Logo Here</h1>
            </div>
            <div class="col-md-5 p-3 ">
                <h1 class="text-center">THANK YOU</h1>
                <h2 class="text-center">FOR YOUR SIGNUP REQUEST</h2>
                <p class="text-center text-muted">Please provide us with some additional information <br> <span>in order to complete your profile</span></p>

                <div class="line mt-5">
                    <div class="position-relative">
                        <div class="position-absolute circle-positiin">
                           <div class="d-flex justify-content-center align-items-center flex-column">
                            <div class="circle">
                                <div class="d-flex justify-content-center align-items-center h-100">
                                    <p class="text-white text-center mb-0 fw-bold">01</p>
                                </div>
                            </div>
                            <p class="text-primary mt-2 detail">login detail</p>
                           </div>
                        </div>
                    </div>
                    <div class="position-relative">
                        <div class="position-absolute circle-positiin-2">
                        <div class="d-flex justify-content-center align-items-center flex-column">
                            <div class="circle-2">
                                <div class="d-flex justify-content-center align-items-center h-100">
                                    <p class="text-white text-center mb-0 fw-bold">02</p>
                                </div>
                            </div>
                            <p class="text-Success mt-2 user">user infotmation</p>
                        </div>
                        </div>
                    </div>
                    <div class="position-relative">
                        <div class="position-absolute circle-positiin-3">
                          <div class="d-flex justify-content-center align-items-center flex-column">
                            <div class="circle-3">
                                <div class="d-flex justify-content-center align-items-center h-100">
                                    <p class="text-white text-center mb-0 fw-bold">03</p>
                                </div>
                            </div>
                            <p class="address text-muted">physical address</p>
                          </div>
                        </div>
                    </div>
                </div>

               <div class=" form_section">
                    <!-- form -->
                <form class="row g-3 sign-up">
                    <div class="col-md-4">
                      <!-- <input type="text" class="form-control" id="inputEmail4" placeholder="Month"> -->
                      <input type="text" class="form-control rounded-0 mb-3 text-uppercase ps-0" id="exampleFormControlInput1" placeholder="Month">
                    </div>
                    <div class="col-md-4">
                      <input type="text" class="form-control" id="" placeholder="Day">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id=""placeholder="Year">
                      </div>
                    <div class="col-12">
                      <input type="text" class="form-control" id="" placeholder="ID Type*">
                    </div>
                    <div class="col-12 ">
                      <input type="number" class="form-control" id="" placeholder="ID Number">
                    </div>
                    <div class="col-md-12">
                      <input type="text" class="form-control" id="" placeholder="Country of Residence">
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" id="" placeholder="Country of Citizenship">
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <a href="#" class="text-black-50">Continue Later</a>
                           </div>
                         <div class="col-md-6">
                           <button type="submit" class="btn btn-primary">Continue</button>
                    </div>
                    </div>
                  </form>
               </div>
            </div>
        </div>
    </div>
</section>
@endsection
